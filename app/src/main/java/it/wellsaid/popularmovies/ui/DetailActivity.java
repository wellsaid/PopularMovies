package it.wellsaid.popularmovies.ui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import it.wellsaid.popularmovies.R;
import it.wellsaid.popularmovies.utils.FavoriteMoviesUtils;
import it.wellsaid.popularmovies.utils.Movie;
import it.wellsaid.popularmovies.utils.MovieReview;
import it.wellsaid.popularmovies.utils.MovieReviewsAdapter;
import it.wellsaid.popularmovies.utils.MovieTrailer;
import it.wellsaid.popularmovies.utils.MovieTrailersAdapter;
import it.wellsaid.popularmovies.utils.TheMovieDB;

public class DetailActivity extends AppCompatActivity implements TheMovieDB.MovieDetailListener {

    public static final String MOVIE_BUNDLE_KEY="movie";

    /* Define view bindings */
    @BindView(R.id.detail_sv) ScrollView detailScrollView;
    @BindView(R.id.detail_tool_bar) Toolbar detailToolbar;
    @BindView(R.id.poster_iv) ImageView posterImageView;
    @BindView(R.id.release_date_tv) TextView releaseDateTextView;
    @BindView(R.id.vote_avg_tv) TextView voteAverageTextView;
    @BindView(R.id.plot_syn_tv) TextView plotSynopsisTextView;
    @BindView(R.id.movie_trailers_rv) RecyclerView movieTrailersRecyclerView;
    @BindView(R.id.movie_reviews_rv) RecyclerView movieReviewsRecyclerView;
    MenuItem fav_action;

    /* Object used to retrieve trailers */
    private TheMovieDB theMovieDB;

    /* Object used to add/remove movies in favorites */
    private FavoriteMoviesUtils favUtils = null;

    /* toast showed in the activity */
    private Toast toast = null;

    /* adapter and layout manager of the movie trailers recycler view */
    private MovieTrailersAdapter movieTrailersAdapter;

    /* adapter and layout manager of the movie trailers recycler view */
    private MovieReviewsAdapter movieReviewsAdapter;
    private LinearLayoutManager movieReviewsLayoutManager;

    /* true if a nextMovieReviews() operation is running */
    private boolean nextMovieReviewsOperation = false;

    /* it will contain the movie of this instance */
    private Movie movie;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        /* binding the views */
        ButterKnife.bind(this);

        /* get the movie from parent */
        movie = getIntent().getParcelableExtra(MOVIE_BUNDLE_KEY);

        /* set the title on the toolbar */
        detailToolbar.setTitle(movie.getTitle());

        /* show the toolbar */
        setSupportActionBar(detailToolbar);

        /* Create the object that will add/remove movies from favorites */
        favUtils = new FavoriteMoviesUtils(this);

        /* show poster image */
        posterImageView.setContentDescription(
                getResources().getString(R.string.movie_poster_desc)
                        + ' ' + movie.getTitle()
        );
        favUtils.showPosterInImageView(movie, posterImageView);

        /* Set the image description */
        posterImageView.setContentDescription(
                getString(R.string.movie_poster_desc) + ' ' + movie.getTitle());

        /* Set the release date */
        releaseDateTextView.setText(movie.getReleaseDate());

        /* Set the average vote */
        voteAverageTextView.setText(String.valueOf(movie.getVoteAverage()));

        /* Set the plot synopsis */
        plotSynopsisTextView.setText(movie.getPlotSynopsis());

        /* improves performance: changes in content do not change
         * the layout size of the RecyclerView */
        movieTrailersRecyclerView.setHasFixedSize(true);
        movieReviewsRecyclerView.setHasFixedSize(true);

        /* create manager (as a linear layout one) for the movie trailers
         * recycle view */
        movieTrailersRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        /* create the adapter for the movie trailers recycle view */
        movieTrailersAdapter = new MovieTrailersAdapter(this,null);
        movieTrailersRecyclerView.setAdapter(movieTrailersAdapter);

        /* create manager (as a linear layout one) for the movie reviews
         * recycle view */
        movieReviewsLayoutManager = new LinearLayoutManager(this);
        movieReviewsRecyclerView.setLayoutManager(movieReviewsLayoutManager);

        /* create the adapter for the movie reviews recycle view */
        movieReviewsAdapter = new MovieReviewsAdapter(null);
        movieReviewsRecyclerView.setAdapter(movieReviewsAdapter);

        /* set on scroll listener for the movie reviews recycle view */
        movieReviewsRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int scrollState) {
                super.onScrollStateChanged(recyclerView, scrollState);
                /* if the reviews recycle view has hit the bottom ... */
                if(scrollState == RecyclerView.SCROLL_STATE_IDLE &&
                        movieReviewsLayoutManager.findLastCompletelyVisibleItemPosition() ==
                                movieReviewsAdapter.getItemCount()-1){
                    /* ... if a nextMovieReviews() operation is not running ... */
                    if(!nextMovieReviewsOperation) {
                        /* ... load another page (if exists) */
                        theMovieDB.nextMovieReviews(movie.getID());
                        /* a nextMovieReviews() operation is running... */
                        nextMovieReviewsOperation = true;
                    }
                }
            }
        });

        /* Create the object that will retrieve trailers and reviews */
        theMovieDB = new TheMovieDB(this,this);

        /* retrieve the trailers and (first page of) reviews lists for this movie */
        theMovieDB.getMovieTrailers(movie.getID());
        theMovieDB.nextMovieReviews(movie.getID());
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        fav_action = menu.findItem(R.id.action_favorite);

        /* if movie is favorite ... */
        if(favUtils.movieIsFavorite(movie.getID())){
            /* ... set action icon to on */
            fav_action.setIcon(android.R.drawable.btn_star_big_on);
        }

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        /* inflate the actions in the action bar */
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.detail_toolbar_actions, menu);

        return true;
    }

    @Override
    public void onMovieTrailerListAvailable(List<MovieTrailer> movieTrailers) {
        if(movieTrailers == null){
            /* there was some error in retrieving movie posters list */
            if(toast != null){
                toast.cancel();
            }

            toast = Toast.makeText(this,
                    getResources().getString(R.string.retrieving_error_toast),
                    Toast.LENGTH_LONG);
            toast.show();

            return;
        }

        /* Add them to the trailers recycle view */
        movieTrailersAdapter.add(movieTrailers);
    }

    @Override
    public void onMovieReviewListAvailable(List<MovieReview> movieReviews) {
        /* ... the nextMovieReviews() operation as stopped */
        nextMovieReviewsOperation = false;

        if(movieReviews == null){
            /* there was some error in retrieving movie posters list */
            if(toast != null){
                toast.cancel();
            }

            toast = Toast.makeText(this,
                    getResources().getString(R.string.retrieving_error_toast),
                    Toast.LENGTH_LONG);
            toast.show();

            return;
        }

        /* Add them to the reviews recycle view */
        movieReviewsAdapter.add(movieReviews);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_favorite:
                /* If the movie was favorite ... */
                if(favUtils.movieIsFavorite(movie.getID())) {
                    /* ... remove it from favorites ... */
                    favUtils.remove(movie);

                    /* ... set action icon to off */
                    fav_action.setIcon(android.R.drawable.btn_star_big_off);

                /* .. otherwise ... */
                } else {
                    /* ... save the movie has favorite ... */
                    favUtils.add(movie);

                    /* ... set action icon to on */
                    fav_action.setIcon(android.R.drawable.btn_star_big_on);
                }
            default:
                /* User's action was not recognized. Invoke the superclass to handle it. */
                return super.onOptionsItemSelected(item);
        }
    }
}
