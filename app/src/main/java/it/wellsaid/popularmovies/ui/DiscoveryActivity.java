package it.wellsaid.popularmovies.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v7.widget.Toolbar;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import it.wellsaid.popularmovies.R;
import it.wellsaid.popularmovies.utils.FavoriteMoviesContract;
import it.wellsaid.popularmovies.utils.FavoriteMoviesProvider;
import it.wellsaid.popularmovies.utils.Movie;
import it.wellsaid.popularmovies.utils.MovieAdapter;
import it.wellsaid.popularmovies.utils.TheMovieDB;

public class DiscoveryActivity extends AppCompatActivity
        implements TheMovieDB.MovieListListener {

    /* Define view bindings */
    @BindView(R.id.disc_tool_bar) Toolbar discoverToolbar;
    @BindView(R.id.movies_poster_gv) GridView gridView;
    @BindView(R.id.no_data_tv) TextView noDataTV;

    /* toast showed in the activity */
    private Toast toast = null;

    /* The adapter to control content of the grid view */
    private MovieAdapter moviesAdapter;

    /* Object used to retrieve movies */
    private TheMovieDB theMovieDB;

    /* The request to make */
    private String request;

    /* The shared preference object to read/write preferences */
    private SharedPreferences sharedPreferences;

    /* true if a nextMovies() operation is running */
    private boolean nextMoviesOperation = false;

    /* number of favorite movies to retrieve */
    private int numFavMovies = 0;
    /* list of favorite movies retrieved */
    private List<Movie> favMovies = null;

    /* network state receiver */
    private BroadcastReceiver networkStateReceiver = null;
    private boolean networkStateReceiverRegistered = false;

    /* contains the previous state of the connection */
    private boolean network;

    /* returns true if we have an internet connection
     * source: https://stackoverflow.com/questions/4238921/detect-whether-there-is-an-internet-connection-available-on-android#4239019
     */
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private void onSortItemChanged(){
        /* saving it in the shared preference file */
        sharedPreferences.edit()
                .putString(getResources().getString(R.string.sort_order_key),request)
                .apply();

        /* if we have no internet connection and the request is not favorite movies ... */
        if(!isNetworkAvailable() && !request.equals(FavoriteMoviesProvider.FAVORITE_MOVIES)){
            /* ... hide grid view ... */
            gridView.setVisibility(View.GONE);
            /* ... show no data text view ... */
            noDataTV.setVisibility(View.VISIBLE);
            /* ... stop here */
            return;
        } else {
            /* ... show grid view ... */
            gridView.setVisibility(View.VISIBLE);
            /* ... hide no data text view ... */
            noDataTV.setVisibility(View.GONE);
        }

        /* clean the grid view + reset page */
        moviesAdapter.clear();
        theMovieDB.resetPage();

        /* if the request is to display the favorite movies... */
        if(request.equals(FavoriteMoviesProvider.FAVORITE_MOVIES)){
            /* ... retrieve list of favorite movie IDs from provider ... */
            Cursor c = getContentResolver().query(
                    FavoriteMoviesContract.Entry.CONTENT_URI,
                    null,
                    null,
                    null,
                    null
            );

            /* if we have some favorite movies ... */
            if (c != null && c.getCount() > 0){
                /* ... save how many of them there are ... */
                numFavMovies = c.getCount();
                /* ... clean favorite movies ... */
                favMovies = null;

                /* ... for each favorite movie ... */
                c.moveToFirst();
                do {
                    /* ... get id of the movie ... */
                    int id = c.getInt(c.getColumnIndex(FavoriteMoviesContract.Entry._ID));

                    /* ... if we have connection ... */
                    if(isNetworkAvailable()) {
                        /* ... get info from the server */
                        theMovieDB.getMovie(id);
                    /* ... otherwise (we do not have connection) ... */
                    } else {
                        /* ... build the movie object ... */
                        String title =
                                c.getString(c.getColumnIndex(FavoriteMoviesContract.Entry.TITLE));
                        String releaseDate =
                                c.getString(c.getColumnIndex(FavoriteMoviesContract.Entry.RELEASE_DATE));
                        float voteAverage =
                                c.getFloat(c.getColumnIndex(FavoriteMoviesContract.Entry.VOTE_AVERAGE));
                        String plotSynopsis =
                                c.getString(c.getColumnIndex(FavoriteMoviesContract.Entry.PLOT_SYNOPSIS));
                        Movie movie = new Movie(id, title, releaseDate, null,
                                voteAverage, plotSynopsis);

                        /* ... add it to the adapter */
                        moviesAdapter.add(movie);
                    }

                    c.moveToNext();
                } while (!c.isAfterLast());

                /* ... finally, close the cursor */
                c.close();
            }

        /* ... otherwise ... */
        } else {
            /* repopulate it with the new sorting order */
            theMovieDB.nextMovies(request);
            /* a nextMovies() operation is running... */
            nextMoviesOperation = true;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /* Inflate the layout */
        setContentView(R.layout.activity_discovery);

        /* binding the views */
        ButterKnife.bind(this);

        /* show the action bar */
        setSupportActionBar(discoverToolbar);

        /* Getting resources and shared preferences */
        Resources resources = getResources();
        sharedPreferences = getSharedPreferences(
                resources.getString(R.string.conf_file), Context.MODE_PRIVATE);

        /* Retrieve sorting order exists from preferences */
        request = sharedPreferences.getString(resources.getString(R.string.sort_order_key), "");

        /* Create the object that will retrieve the movies */
        theMovieDB = new TheMovieDB(this,this);

        /* if it do not exists */
        if(request.equals("")){
            /* set it to default value */
            request = theMovieDB.POPULAR_MOVIES;
            sharedPreferences.edit().putString(resources.getString(R.string.sort_order_key),
                    theMovieDB.POPULAR_MOVIES).apply();
        }

        /* Create the MoviePostersAdapter */
        moviesAdapter = new MovieAdapter(this);

        /* set the gridView adapter to out create adapter */
        gridView.setAdapter(moviesAdapter);

        /* set on click listener for movie posters */
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                /* Getting the movie the user clicked on */
                Movie movie = moviesAdapter.getItem(i);

                /* Create the intent to start the activity */
                Intent intent = new Intent(DiscoveryActivity.this, DetailActivity.class);

                /* Prepare the bundle for the detail activity */
                intent.putExtra(DetailActivity.MOVIE_BUNDLE_KEY, movie);

                /* start the detail activity */
                startActivity(intent);
            }
        });

        /* set on scroll listener for the movie grid view */
        gridView.setOnScrollListener(new GridView.OnScrollListener(){
            @Override
            public void onScrollStateChanged(AbsListView absListView, int scrollState) {
                /* if we are currently looking at favorite movies... */
                if(request.equals(FavoriteMoviesProvider.FAVORITE_MOVIES)){
                    /* ... stop this behavior */
                    return;
                }

                /* if the grid view has hit the bottom ... */
                if (scrollState == GridView.OnScrollListener.SCROLL_STATE_IDLE &&
                        gridView.getLastVisiblePosition() == gridView.getAdapter().getCount()-1) {
                    /* ... if a nextMovies() operation is not running ... */
                    if(!nextMoviesOperation) {
                        /* ... load another page (if exists) */
                        theMovieDB.nextMovies(request);
                        /* a nextMovies() operation is running... */
                        nextMoviesOperation = true;
                    }
                }
            }

            @Override
            public void onScroll(AbsListView absListView, int i, int i1, int i2) {

            }
        });

        /* create network state receiver
        *  source: https://stackoverflow.com/questions/6169059/android-event-for-internet-connectivity-state-change#25873554
        * */
        networkStateReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if(intent == null || intent.getExtras() == null)
                    return;

                /* if connection changed to on ... */
                if(isNetworkAvailable()) {
                    /* ... if it was off ... */
                    if(!network) {
                        /* ... reload ui ... */
                        onSortItemChanged();
                    }

                    /* ... change previous connection state */
                    network = true;
                /* ... otherwise, if connection went off */
                } else if(intent.getBooleanExtra(
                        ConnectivityManager.EXTRA_NO_CONNECTIVITY,Boolean.FALSE)) {
                    network = false;
                }
            }
        };

        /* get connection status */
        network = isNetworkAvailable();
    }

    @Override
    protected void onResume() {
        /* register our network state receiver */
        if(!networkStateReceiverRegistered) {
            this.registerReceiver(networkStateReceiver,
                    new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));

            networkStateReceiverRegistered = true;
        }

        /* If first time: show movies to the user */
        /* If coming from detail activity: reload movies */
        if(moviesAdapter.getCount() == 0 ||
                request.equals(FavoriteMoviesProvider.FAVORITE_MOVIES)) {
            onSortItemChanged();
        }

        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();

        /* unregister our network state receiver */
        if(networkStateReceiverRegistered) {
            this.unregisterReceiver(networkStateReceiver);

            networkStateReceiverRegistered = false;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        /* unregister our network state receiver */
        if(networkStateReceiverRegistered) {
            this.unregisterReceiver(networkStateReceiver);

            networkStateReceiverRegistered = false;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        /* inflate the actions in the action bar */
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.discovery_toolbar_actions, menu);
        return true;
    }

    @Override
    public void onMoviePostersListAvailable(List<Movie> movies) {
        /* ... the nextMovies() operation as stopped */
        nextMoviesOperation = false;

        if(movies == null){
            /* there was some error in retrieving movie posters list */
            if(toast != null){
                toast.cancel();
            }

            toast = Toast.makeText(this,
                    getResources().getString(R.string.retrieving_error_toast),
                    Toast.LENGTH_LONG);
            toast.show();

            return;
        }

        /* add new movie posters */
        moviesAdapter.addAll(movies);
    }

    @Override
    public void onMovieAvailable(Movie movie){
        if(movie == null){
            /* there was some error in retrieving movie */
            if(toast != null){
                toast.cancel();
            }

            toast = Toast.makeText(this,
                    getResources().getString(R.string.retrieving_error_toast),
                    Toast.LENGTH_LONG);
            toast.show();

            return;
        }

        if(favMovies == null){
            favMovies = new ArrayList<>();
        }

        /* add the movie to the list of favorite movies */
        favMovies.add(movie);
        numFavMovies--;

        /* when we receive the last movie ... */
        if(numFavMovies == 0){
            /* ... fill the grid view */
            onMoviePostersListAvailable(favMovies);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_sort_order:
                /* nothing to do: menu will popup */
                return true;
            case R.id.item_popular_sort:
                request = theMovieDB.POPULAR_MOVIES;
                onSortItemChanged();
                return true;
            case R.id.item_top_rated_sort:
                request = theMovieDB.TOP_RATED_MOVIES;
                onSortItemChanged();
                return true;
            case R.id.item_favorite_sort:
                request = FavoriteMoviesProvider.FAVORITE_MOVIES;
                onSortItemChanged();
                return true;
            default:
                /* User's action was not recognized. Invoke the superclass to handle it. */
                return super.onOptionsItemSelected(item);
        }
    }
}