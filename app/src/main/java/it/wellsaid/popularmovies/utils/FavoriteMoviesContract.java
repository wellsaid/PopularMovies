package it.wellsaid.popularmovies.utils;

import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

/* Adapted from the following example:
 *  https://github.com/udacity/android-content-provider
 */

public class FavoriteMoviesContract {

    /* a symbolic name for the entire provider  */
    static final String CONTENT_AUTHORITY = "it.wellsaid.popularmovies.app";

    /* the URI for the base app */
    private static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    /* tables name */
    static final String TABLE_FAV_MOVIES = "fav_movies";

    public static final class Entry
            /* colmun _ID will be used to store favorite movie ID */
            implements BaseColumns {

        /* attributes in the table */
        public static final String TITLE = "title";
        public static final String RELEASE_DATE = "release_date";
        public static final String VOTE_AVERAGE = "vote_average";
        public static final String PLOT_SYNOPSIS = "plot_synopsis";
        public static final String HAS_POSTER_IMAGE = "has_poster_image";

        /* create content uri */
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon()
                .appendPath(TABLE_FAV_MOVIES).build();

        // for building URIs on insertion
        static Uri buildFavoriteMoviesUri(long id){
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }
    }
}

