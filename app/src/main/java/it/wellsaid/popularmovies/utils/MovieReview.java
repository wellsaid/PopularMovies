package it.wellsaid.popularmovies.utils;

import android.os.Parcel;
import android.os.Parcelable;

public class MovieReview implements Parcelable {

    /* MovieReview attributes */
    private final String author;
    private final String content;

    public static final Parcelable.Creator<MovieReview> CREATOR =
            new Parcelable.Creator<MovieReview>(){

                /* used to convert the parcelled object back to a Movie */
                @Override
                public MovieReview createFromParcel(Parcel src) {
                    /* get all attributes from source parcelable (in the same order) */
                    String author = src.readString();
                    String content = src.readString();

                    /* return a new constructed MovieTrailer object */
                    return new MovieReview(author, content);
                }

                @Override
                public MovieReview[] newArray(int i) {
                    return new MovieReview[0];
                }
    };

    MovieReview(final String author, final String content){
        this.author = author;
        this.content = content;
    }

    public String getAuthor(){
        return author;
    }

    public String getContent(){
        return content;
    }

    @Override
    public int describeContents() {
        /* no file descriptors included */
        return 0;
    }

    /* Called to parcel the MovieReview into a Bundle */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        /* write all attributes in destination parcelable */
        dest.writeString(author);
        dest.writeString(content);
    }

}
