package it.wellsaid.popularmovies.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.FileOutputStream;

import it.wellsaid.popularmovies.R;

public class FavoriteMoviesUtils {

    private final Context context;

    public FavoriteMoviesUtils(Context context){
        this.context = context;
    }

    /* returns true if we have an internet connection
     * source: https://stackoverflow.com/questions/4238921/detect-whether-there-is-an-internet-connection-available-on-android#4239019
     */
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    /*
     * It returns true if the movie with the passed id is favorite
     * false otherwise
     */
    public boolean movieIsFavorite(int id){
        /* Query the database and store data in a cursor */
        Cursor c = context.getContentResolver().query(
                FavoriteMoviesContract.Entry.CONTENT_URI,
                new String[]{FavoriteMoviesContract.Entry._ID},
                FavoriteMoviesContract.Entry._ID + '=' + id,
                null,
                null
        );

        /* return true if the cursor is not empty */
        boolean toRet =  (c.getCount() > 0);
        c.close();

        return toRet;
    }

    /*
     * It returns true if the movie has poster offline
     * false otherwise
     */
    private boolean movieHasPoster(int id){
        /* Query the database and store data in a cursor */
        Cursor c = context.getContentResolver().query(
                FavoriteMoviesContract.Entry.CONTENT_URI,
                new String[]{FavoriteMoviesContract.Entry.HAS_POSTER_IMAGE},
                FavoriteMoviesContract.Entry._ID + '=' + id,
                null,
                null
        );

        /* return true if the movie exists and it has offline poster, false otherwise */
        boolean toRet = false;
        if(c.getCount() == 1){
            c.moveToFirst();
            toRet = c.getInt(c.getColumnIndex(FavoriteMoviesContract.Entry.HAS_POSTER_IMAGE)) == 1;
        }

        c.close();
        return toRet;
    }

    /* Adds a movie to favorites through the content provider */
    private void finalAddMovieToFavorites(Movie movie, boolean hasPosterImage){
        final ContentValues val = new ContentValues();
        val.put(FavoriteMoviesContract.Entry._ID, movie.getID());
        val.put(FavoriteMoviesContract.Entry.TITLE, movie.getTitle());
        val.put(FavoriteMoviesContract.Entry.RELEASE_DATE, movie.getReleaseDate());
        val.put(FavoriteMoviesContract.Entry.VOTE_AVERAGE, movie.getVoteAverage());
        val.put(FavoriteMoviesContract.Entry.PLOT_SYNOPSIS, movie.getPlotSynopsis());
        val.put(FavoriteMoviesContract.Entry.HAS_POSTER_IMAGE, hasPosterImage);

        context.getContentResolver().insert(FavoriteMoviesContract.Entry.CONTENT_URI, val);
    }

    /*
     * Performs pre-saving computation and calls final adder
     */
    public void add(final Movie movie){
        /* ... if we have a connection ... */
        if(isNetworkAvailable()){
            /* ... download the image and save it in the application data dir ... */
            Picasso.with(context)
                    .load(movie.getPosterImageURL())
                    .into(new Target() {
                        @Override
                        public void onBitmapLoaded(final Bitmap bitmap,
                                                   Picasso.LoadedFrom from) {
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    String path = context.getApplicationInfo().dataDir +
                                            File.separator +
                                            context.getResources().getString(
                                                    R.string.poster_image_dir) +
                                            File.separator;
                                    File file = new File(path);

                                    file.mkdirs();
                                    file = new File(path + movie.getID() +
                                            context.getResources().getString(
                                                    R.string.poster_image_ext));

                                    try {
                                        file.createNewFile();
                                        FileOutputStream ostream =
                                                new FileOutputStream(file);
                                        bitmap.compress(Bitmap.CompressFormat.JPEG,
                                                100, ostream);

                                        ostream.flush();
                                        ostream.close();
                                    }
                                    catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }).start();

                            /* final add movie to favorites with poster image */
                            finalAddMovieToFavorites(movie, true);
                        }

                        @Override
                        public void onBitmapFailed(Drawable errorDrawable) {
                            /* final add movie to favorites without poster image */
                            finalAddMovieToFavorites(movie, false);
                        }

                        @Override
                        public void onPrepareLoad(Drawable placeHolderDrawable) {

                        }
                    });
                    /* ... otherwise ... */
        } else {
            /* final add movie to favorites without poster image */
            finalAddMovieToFavorites(movie, false);
        }
    }

    /* Adds a movie to favorites through the content provider */
    private void finalRemoveMovieToFavorites(Movie movie){
        context.getContentResolver().delete(
                FavoriteMoviesContract.Entry.CONTENT_URI,
                FavoriteMoviesContract.Entry._ID + '=' + movie.getID(),
                null
        );
    }

    /*
     * Performs pre-clean computation and calls final remover
     */
    public void remove(final Movie movie){
        /* if movie has poster saved ... */
        if(movieHasPoster(movie.getID())){
            /* ... remove it from directory */
            String path = context.getApplicationInfo().dataDir + File.separator +
                    context.getResources().getString(R.string.poster_image_dir) +
                    File.separator + movie.getID() +
                    context.getResources().getString(R.string.poster_image_ext);
            File file = new File(path);

            file.delete();
        }

        /* final remove movie from favorites */
        finalRemoveMovieToFavorites(movie);
    }

    /*
     * returns the path to the eventual movie poster path
     */
    private String getMoviePosterPath(final int ID){
        return context.getApplicationInfo().dataDir + File.separator +
                context.getResources().getString(R.string.poster_image_dir) +
                File.separator + ID +
                context.getResources().getString(R.string.poster_image_ext);
    }

    /*
     * shows the movie poster in the passed image view from url (if we have it)
     * or from local storage
     */
    public void showPosterInImageView(final Movie movie, final ImageView imageView){
        /* if we have an online url for the poster ... */
        if(movie.getPosterImageURL() != null) {
            /* .. show it from the url */
            Picasso.with(context)
                    .load(movie.getPosterImageURL())
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder_error)
                    .into(imageView);
        } else {
            /* ... try to show it from local storage */
            String path = getMoviePosterPath(movie.getID());

            Picasso.with(context)
                    .load(new File(path))
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder_error)
                    .into(imageView);
        }
    }
}
