package it.wellsaid.popularmovies.utils;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import java.util.ArrayList;

import it.wellsaid.popularmovies.R;

/* The code for this class has been adapted from this example:
 *  https://github.com/udacity/android-custom-arrayadapter/blob/master/app/src/main/java \
 *  /demo/example/com/customarrayadapter/AndroidFlavorAdapter.java
 */

public class MovieAdapter extends ArrayAdapter<Movie> {


    private final Context context;

    public MovieAdapter(@NonNull Context context) {
        /* The second argument is 0 because we do not want to populate simply a text view */
        super(context, 0, new ArrayList<Movie>());

        this.context = context;
    }

    /* We use this method to provide a view to our adapter */
    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        /* Gets the Movie object from the ArrayAdapter at the appropriate position */
        Movie movie = getItem(position);

        /* If this is a new View object we're getting, then inflate the layout. */
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext())
                    .inflate(R.layout.movie_poster, parent, false);
        }

        /* If not, this view already has the layout inflated from a previous call to getView,
           and we modify the View widgets as usual. */
        ImageView posterView = convertView.findViewById(R.id.movie_poster);
        posterView.setContentDescription(
                context.getResources().getString(R.string.movie_poster_desc)
                 + ' ' + movie.getTitle()
        );

        /* show poster image */
        (new FavoriteMoviesUtils(context)).showPosterInImageView(movie, posterView);

        return convertView;
    }
}