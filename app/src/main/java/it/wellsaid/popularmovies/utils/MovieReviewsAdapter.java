package it.wellsaid.popularmovies.utils;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import it.wellsaid.popularmovies.R;

public class MovieReviewsAdapter
        extends android.support.v7.widget.RecyclerView.Adapter<MovieReviewsAdapter.ViewHolder>{

    /* The dataset of this adapter */
    private final List<MovieReview> movieReviews;

    /* A reference to the views for each data item in a view holder */
    static class ViewHolder extends RecyclerView.ViewHolder {

        /* Define view bindings */
        @BindView(R.id.author_tv) TextView authorTV;
        @BindView(R.id.content_tv) TextView contentTV;

        ViewHolder(View v) {
            super(v);

            /* Binding the views */
            ButterKnife.bind(this, v);
        }

    }

    public MovieReviewsAdapter(List<MovieReview> movieReviews){
        /* if a list as been given at construction ... */
        if(movieReviews != null){
            /* ... initialize adapter with it */
            this.movieReviews = movieReviews;
        /* ... otherwise ... */
        } else {
            /* ... create a new empty one */
            this.movieReviews = new ArrayList<>();
        }
    }

    /* Create new views (invoked by the layout manager) */
    @Override
    @NonNull
    public MovieReviewsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
                                                             int viewType) {
        /* inflate elements layout */
        View v =  LayoutInflater.from(parent.getContext())
                .inflate(R.layout.movie_review_elem, parent, false);

        /* create a view holder to fill the new elements and return it to the caller */
        return new ViewHolder(v);
    }

    /* Replace the contents of a view (invoked by the layout manager) */
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        /* get element from your dataset at this position and
         * fill the contents of the view with that element */
        holder.authorTV.setText(movieReviews.get(position).getAuthor());
        holder.contentTV.setText(movieReviews.get(position).getContent());
    }

    /* Returns the size of your dataset (invoked by the layout manager) */
    @Override
    public int getItemCount() {
        return movieReviews.size();
    }

    /* Called by the parent activity when it wants to add some trailers */
    public void add(List<MovieReview> toAdd){
        movieReviews.addAll(toAdd);
        notifyDataSetChanged();
    }
}