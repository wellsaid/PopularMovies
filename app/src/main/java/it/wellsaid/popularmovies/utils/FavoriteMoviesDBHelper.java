package it.wellsaid.popularmovies.utils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/* Adapted from the following example:
 *  https://github.com/udacity/android-content-provider
 */

class FavoriteMoviesDBHelper extends SQLiteOpenHelper {

    /* name & version of the database */
    private static final String DATABASE_NAME = "fav_movies.db";
    private static final int DATABASE_VERSION = 4;

    FavoriteMoviesDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        /* build database creation query */
        final String SQL_CREATE_MOVIE_TABLE = "CREATE TABLE " +
                FavoriteMoviesContract.TABLE_FAV_MOVIES + "(" +
                FavoriteMoviesContract.Entry._ID + " INTEGER PRIMARY KEY, " +
                FavoriteMoviesContract.Entry.TITLE + " TEXT, " +
                FavoriteMoviesContract.Entry.RELEASE_DATE + " TEXT, " +
                FavoriteMoviesContract.Entry.VOTE_AVERAGE + " FLOAT, " +
                FavoriteMoviesContract.Entry.PLOT_SYNOPSIS + " TEXT, " +
                FavoriteMoviesContract.Entry.HAS_POSTER_IMAGE + " BOOLEAN);";

        /* execute the query */
        sqLiteDatabase.execSQL(SQL_CREATE_MOVIE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        /* drop the movies table */
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " +
                FavoriteMoviesContract.TABLE_FAV_MOVIES);

        /* recreate the database */
        onCreate(sqLiteDatabase);
    }
}

