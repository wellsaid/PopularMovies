package it.wellsaid.popularmovies.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import it.wellsaid.popularmovies.R;

public class MovieTrailersAdapter
        extends android.support.v7.widget.RecyclerView.Adapter<MovieTrailersAdapter.ViewHolder>{

    /* The dataset of this adapter */
    private final List<MovieTrailer> movieTrailers;

    /* The context from which this adapter is used
     * (used to start an intent)
     */
    private final Context context;

    /* A reference to the views for each data item in a view holder */
    static class ViewHolder extends RecyclerView.ViewHolder {

        /* Define view bindings */
        @BindView(R.id.ytb_icon_iv) ImageView ytbIV;
        @BindView(R.id.trailer_name_tv) TextView trailerNameTV;
        @BindView(R.id.share_icon_iv) ImageView shareIV;

        ViewHolder(View v) {
            super(v);

            /* Binding the views */
            ButterKnife.bind(this, v);
        }

    }

    public MovieTrailersAdapter(Context context, List<MovieTrailer> movieTrailers){
        /* if a list as been given at construction ... */
        if(movieTrailers != null){
            /* ... initialize adapter with it */
            this.movieTrailers = movieTrailers;
        /* ... otherwise ... */
        } else {
            /* ... create a new empty one */
            this.movieTrailers = new ArrayList<>();
        }

        this.context = context;
    }

    /* Create new views (invoked by the layout manager) */
    @Override
    @NonNull
    public MovieTrailersAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
                                                              int viewType) {
        /* inflate elements layout */
        View v =  LayoutInflater.from(parent.getContext())
                .inflate(R.layout.movie_trailer_elem, parent, false);

        /* create a view holder to fill the new elements and return it to the caller */
        return new ViewHolder(v);
    }

    /* Replace the contents of a view (invoked by the layout manager) */
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        /* get element from your dataset at this position and
         * fill the contents of the view with that element */
        holder.trailerNameTV.setText(movieTrailers.get(position).getName());

        /* set on click listener for element */
        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /* Sending an intent to open the trailer URL */
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(movieTrailers
                        .get(holder.getAdapterPosition()).getURL()));
                context.startActivity(intent);
            }
        };
        holder.trailerNameTV.setOnClickListener(onClickListener);
        holder.ytbIV.setOnClickListener(onClickListener);

        /* set on click listener for sharing */
        View.OnClickListener sharingOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /* Sending an intent to share the trailer URL */
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                MovieTrailer movie = movieTrailers.get(holder.getAdapterPosition());
                intent.putExtra(Intent.EXTRA_TEXT, movie.getURL());
                context.startActivity(
                        Intent.createChooser(intent,
                                context.getResources().getString(R.string.share_intent_title)));
            }
        };
        holder.shareIV.setOnClickListener(sharingOnClickListener);
    }

    /* Returns the size of your dataset (invoked by the layout manager) */
    @Override
    public int getItemCount() {
        return movieTrailers.size();
    }

    /* Called by the parent activity when it wants to add some trailers */
    public void add(List<MovieTrailer> toAdd){
        movieTrailers.addAll(toAdd);
        notifyDataSetChanged();
    }
}
