package it.wellsaid.popularmovies.utils;

import android.os.Parcel;
import android.os.Parcelable;

public class MovieTrailer implements Parcelable {

    /* MovieTrailer attributes */
    private final String name;
    private final String URL;

    public static final Parcelable.Creator<MovieTrailer> CREATOR =
            new Parcelable.Creator<MovieTrailer>(){

        /* used to convert the parcelled object back to a Movie */
        @Override
        public MovieTrailer createFromParcel(Parcel src) {
            /* get all attributes from source parcelable (in the same order) */
            String description = src.readString();
            String URL = src.readString();

            /* return a new constructed MovieTrailer object */
            return new MovieTrailer(description, URL);
        }

        @Override
        public MovieTrailer[] newArray(int i) {
                    return new MovieTrailer[0];
                }
    };

    MovieTrailer(final String description, final String URL){
        this.name = description;
        this.URL = URL;
    }

    public String getName(){
        return name;
    }

    public String getURL(){
        return URL;
    }

    @Override
    public int describeContents() {
        /* no file descriptors included */
        return 0;
    }

    /* Called to parcel the MovieTrailer into a Bundle */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        /* write all attributes in destination parcelable */
        dest.writeString(name);
        dest.writeString(URL);
    }

}
