package it.wellsaid.popularmovies.utils;

import android.os.Parcel;
import android.os.Parcelable;

public class Movie implements Parcelable {

    /* Movie attributes */
    private final int ID;
    private final String title;
    private final String releaseDate;
    private final String posterImageURL;
    private final float voteAverage;
    private final String plotSynopsis;

    public static final Parcelable.Creator<Movie> CREATOR =
            new Parcelable.Creator<Movie>(){

                /* used to convert the parcelled object back to a Movie */
                @Override
                public Movie createFromParcel(Parcel src) {
                    /* get all attributes from source parcelable (in the same order) */
                    int ID = src.readInt();
                    String title = src.readString();
                    String releaseDate = src.readString();
                    String posterImageURL = src.readString();
                    float voteAverage = src.readFloat();
                    String plotSynopsis = src.readString();

                    /* return a new constructed Movie object */
                    return new Movie(ID, title, releaseDate,
                            posterImageURL, voteAverage, plotSynopsis);
                }

                @Override
                public Movie[] newArray(int i) {
                    return new Movie[0];
                }
            };

    public Movie(int ID, String title, String releaseDate, String posterImageURL,
          float voteAverage, String plotSynopsis){
        this.ID = ID;
        this.title = title;
        this.releaseDate = releaseDate;
        this.posterImageURL = posterImageURL;
        this.voteAverage = voteAverage;
        this.plotSynopsis = plotSynopsis;
    }

    public int getID(){
        return ID;
    }

    public String getTitle(){
        return title;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public String getPosterImageURL(){
        return posterImageURL;
    }

    public float getVoteAverage(){
        return voteAverage;
    }

    public String getPlotSynopsis() {
        return plotSynopsis;
    }

    @Override
    public int describeContents() {
        /* no file descriptors included */
        return 0;
    }

    /* Called to parcel the Movie into a Bundle */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        /* write all attributes in destination parcelable */
        dest.writeInt(ID);
        dest.writeString(title);
        dest.writeString(releaseDate);
        dest.writeString(posterImageURL);
        dest.writeFloat(voteAverage);
        dest.writeString(plotSynopsis);
    }
}
