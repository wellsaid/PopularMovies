package it.wellsaid.popularmovies.utils;


import android.content.Context;
import android.content.res.Resources;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import it.wellsaid.popularmovies.R;
import it.wellsaid.popularmovies.net.HTTPAsyncRequest;

public class TheMovieDB {

    private final static String TAG = TheMovieDB.class.getSimpleName();

    /* the class that wants to receive the response must implements this interfaces ... */
    public interface MovieListListener{
        /* ... and override this method */
        void onMoviePostersListAvailable(List<Movie> movies);
        void onMovieAvailable(Movie movie);
    }

    public interface MovieDetailListener{
        /* ... and override this method */
        void onMovieTrailerListAvailable(List<MovieTrailer> movieTrailers);
        void onMovieReviewListAvailable(List<MovieReview> movieReviews);
    }

    /* the listeners to which send the response */
    private MovieListListener movieListListener = null;
    private MovieDetailListener movieDetailListener = null;

    /* base URL to make requests to themoviedb.org */
    private String TMDB_BASE_URL = null;
    /* base URL to get poster images */
    private String POSTER_BASE_URL = null;
    /* the poster size to use */
    private String POSTER_SIZE = null;

    /* parameter keys for themoviedb.org */
    private String API_KEY_PAR = null;
    private String PAGE_KEY_PAR = null;

    /* your themoviedb.org api key */
    private String API_KEY = null;

    /* requests that can be made to themoviedb.org */
    public String POPULAR_MOVIES = null;
    public String TOP_RATED_MOVIES = null;

    /* Next movie list page to retrieve */
    private int movieListPage = 1;
    private int MOVIE_LIST_LAST_PAGE = 1;

    /* Next movie reviews page to retrieve */
    private int movieReviewsPage = 1;
    private int MOVIE_REVIEW_LAST_PAGE = 1;

    /* base URL to make requests to youtube.com */
    private String YTB_BASE_URL = null;

    /* parameters keys for youtube.com */
    private String VIDEO_KEY_PAR = null;

    /* requests that can be made to youtube.com */
    private String WATCH = null;

    /* Resource object to get the strings */
    private Resources resources = null;

    /* define listeners for the two possible requests */
    private final HTTPAsyncRequest.HTTPAsyncRequestListener nextMoviesListener =
            new HTTPAsyncRequest.HTTPAsyncRequestListener(){
                @Override
                public void onServerResponse(String response) {
                    /* If there was an error in retrieving the list of movies */
                    if(response.equals("")){
                        movieListListener.onMoviePostersListAvailable(null);
                        return;
                    }

                    /* Parse json response and build a list of movie posters */
                    List<Movie> movies = new ArrayList<>();

                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        /* if its the first page... */
                        if(movieListPage == 1){
                            /* ... save what is the last page ... */
                            MOVIE_LIST_LAST_PAGE = jsonObject
                                    .getInt(resources.getString(R.string.last_page_json_key));
                        }

                        /* get the json array of the movies in this page */
                        JSONArray pageResultsJsonArray = jsonObject.
                                getJSONArray(resources.getString(R.string.page_results_json_key));
                        /* for each movie in the page */
                        for (int i = 0; i < pageResultsJsonArray.length(); i++) {
                            /* Retrieve all the attributes */
                            JSONObject jsonIterator = (JSONObject) pageResultsJsonArray.get(i);
                            int ID = jsonIterator
                                    .getInt(resources.getString(R.string.id_json_key));
                            String title = jsonIterator
                                    .getString(resources.getString(R.string.title_json_key));
                            String releaseDate = jsonIterator
                                    .getString(resources.getString(R.string.release_date_json_key));
                            String posterImageURL = jsonIterator
                                    .getString(resources.getString(R.string.poster_image_url_json_key));
                            posterImageURL = POSTER_BASE_URL + POSTER_SIZE + posterImageURL;
                            float voteAverage = (float) jsonIterator
                                    .getDouble(resources.getString(R.string.vote_average_json_key));
                            String plotSynopsis = jsonIterator
                                    .getString(resources.getString(R.string.plot_synopsis_json_key));

                            /* Add the movie to the list */
                            movies.add(new Movie(ID, title, releaseDate,
                                    posterImageURL, voteAverage, plotSynopsis));
                        }

                        /* increment page number */
                        movieListPage++;

                    } catch (JSONException e){
                        Log.v(TAG, "Error in parsing json: " + e.getMessage());
                        movieListListener.onMoviePostersListAvailable(null);
                        return;
                    }

                    /* Return it to the listener */
                    movieListListener.onMoviePostersListAvailable(movies);
                }
    };

    private final HTTPAsyncRequest.HTTPAsyncRequestListener movieTrailersListener =
            new HTTPAsyncRequest.HTTPAsyncRequestListener(){
                @Override
                public void onServerResponse(String response) {
                    /* If there was an error in retrieving the movie trailers */
                    if(response.equals("")){
                        movieDetailListener.onMovieTrailerListAvailable(null);
                        return;
                    }

                    /* Parse json response and build a list of movie trailer links */
                    List<MovieTrailer> movieTrailers = new ArrayList<>();

                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        /* get the json array of the movie trailers */
                        JSONArray pageResultsJsonArray = jsonObject.
                                getJSONArray(resources.getString(R.string.page_results_json_key));
                        /* for each trailer */
                        for(int i = 0; i < pageResultsJsonArray.length(); i++) {
                            JSONObject jsonIterator = (JSONObject) pageResultsJsonArray.get(i);

                            /* if this video is a trailer ... */
                            String type = jsonIterator
                                    .getString(resources.getString(R.string.type_json_key));
                            if(type.equals(resources.getString(R.string.trailer_json_val))) {
                                /* ... and is hosted on youtube ... */
                                String site = jsonIterator
                                        .getString(resources.getString(R.string.site_json_key));
                                if (site.equals(resources.getString(R.string.ytb_json_val))) {
                                    /* ... retrieve all attributes ... */
                                    String name = jsonIterator
                                            .getString(resources.getString(R.string.trailer_name_json_key));
                                    String videoKey = jsonIterator
                                            .getString(resources.getString(R.string.trailer_key_json_key));
                                    /* ... build the youtube URL ... */
                                    String URL = YTB_BASE_URL + WATCH + '?' + VIDEO_KEY_PAR
                                                 + '=' + videoKey;

                                    /* ... add the movie trailer to the list */
                                    movieTrailers.add(new MovieTrailer(name, URL.toString()));
                                }
                            }
                        }

                        /* return the list to the listener */
                        movieDetailListener.onMovieTrailerListAvailable(movieTrailers);
                    } catch (JSONException e){
                        Log.v(TAG, "Error in parsing json: " + e.getMessage());
                        movieDetailListener.onMovieTrailerListAvailable(null);
                    }

                }
    };

    private final HTTPAsyncRequest.HTTPAsyncRequestListener nextReviewsListener =
            new HTTPAsyncRequest.HTTPAsyncRequestListener() {
                @Override
                public void onServerResponse(String response) {
                    /* If there was an error in retrieving the movie trailers */
                    if(response.equals("")){
                        movieDetailListener.onMovieReviewListAvailable(null);
                        return;
                    }

                    /* Parse json response and build a list of movie trailer links */
                    List<MovieReview> movieReviews = new ArrayList<>();

                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        /* if its the first page... */
                        if(movieReviewsPage == 1){
                            /* ... save what is the last page ... */
                            MOVIE_REVIEW_LAST_PAGE = jsonObject
                                    .getInt(resources.getString(R.string.last_page_json_key));
                        }

                        /* get the json array of the movie trailers */
                        JSONArray pageResultsJsonArray = jsonObject.
                                getJSONArray(resources.getString(R.string.page_results_json_key));
                        /* for each review */
                        for(int i = 0; i < pageResultsJsonArray.length(); i++) {
                            JSONObject jsonIterator = (JSONObject) pageResultsJsonArray.get(i);

                            /* retrieve all attributes */
                            String author = jsonIterator
                                    .getString(resources.getString(R.string.author_json_key));
                            String content = jsonIterator
                                    .getString(resources.getString(R.string.content_json_key));

                            /* add the movie review to the list */
                            movieReviews.add(new MovieReview(author, content));
                        }

                        /* increment page number */
                        movieReviewsPage++;

                        /* return the list to the listener */
                        movieDetailListener.onMovieReviewListAvailable(movieReviews);
                    } catch (JSONException e){
                        Log.v(TAG, "Error in parsing json: " + e.getMessage());
                        movieDetailListener.onMovieReviewListAvailable(null);
                    }

                }
    };

    private class MovieListener implements HTTPAsyncRequest.HTTPAsyncRequestListener {

        /* the id of the movie the listener is waiting */
        private final int ID;

        MovieListener(int ID){
            this.ID = ID;
        }

        @Override
        public void onServerResponse(String response) {
            /* If there was an error in retrieving the movie */
            if(response.equals("")){
                movieDetailListener.onMovieReviewListAvailable(null);
                return;
            }

            /* Parse json response and build the movie */
            Movie movie;

            try {
                JSONObject jsonObject = new JSONObject(response);

                String title =
                        jsonObject.getString(resources.getString(R.string.title_json_key));
                String releaseDate =
                        jsonObject.getString(resources.getString(R.string.release_date_json_key));
                String posterImageURL = jsonObject
                        .getString(resources.getString(R.string.poster_image_url_json_key));
                posterImageURL = POSTER_BASE_URL + POSTER_SIZE + posterImageURL;
                float voteAverage = (float) jsonObject
                        .getDouble(resources.getString(R.string.vote_average_json_key));
                String plotSynopsis = jsonObject
                        .getString(resources.getString(R.string.plot_synopsis_json_key));


                movie = new Movie(ID, title, releaseDate, posterImageURL,
                            voteAverage, plotSynopsis);

                /* return the list to the listener */
                movieListListener.onMovieAvailable(movie);
            } catch (JSONException e){
                Log.v(TAG, "Error in parsing json: " + e.getMessage());
                movieListListener.onMovieAvailable(null);
            }
        }
    }

    public TheMovieDB(Context context, MovieListListener listener){
        this.movieListListener = listener;

        /* populate needed variables */
        resources = context.getResources();

        TMDB_BASE_URL = resources.getString(R.string.tmdb_base_url);
        POSTER_BASE_URL = resources.getString(R.string.poster_base_url);
        POSTER_SIZE = resources.getString(R.string.poster_size);

        API_KEY_PAR = resources.getString(R.string.api_key_par);
        PAGE_KEY_PAR = resources.getString(R.string.page_key_par);
        API_KEY = resources.getString(R.string.themoviedb_api_key);

        POPULAR_MOVIES = resources.getString(R.string.popular_req);
        TOP_RATED_MOVIES = resources.getString(R.string.top_rated_req);

        if(API_KEY.equals("")){
            Log.v(TAG, "WARNING: themoviedb.org api key not set");
        }
    }

    public TheMovieDB(Context context, MovieDetailListener listener){
        this.movieDetailListener = listener;

        /* populate needed variables */
        resources = context.getResources();

        TMDB_BASE_URL = resources.getString(R.string.tmdb_base_url);

        API_KEY_PAR = resources.getString(R.string.api_key_par);
        API_KEY = resources.getString(R.string.themoviedb_api_key);

        YTB_BASE_URL = resources.getString(R.string.ytb_base_url);

        VIDEO_KEY_PAR = resources.getString(R.string.video_key_par);

        WATCH = resources.getString(R.string.watch_req);

        if(API_KEY.equals("")){
            Log.v(TAG, "WARNING: themoviedb.org api key not set");
        }
    }

    public void resetPage(){
        movieListPage = 1;
    }

    public void nextMovies(final String req){
        /* if we have reached the final page */
        if(movieListPage == MOVIE_LIST_LAST_PAGE+1) {
            /* ... return without doing anything ... */
            return;
        }
        /* ... otherwise, we have to retrieve the new page ... */

        /* ... parsing request url ... */
        String requestURL = /* base url + request */
                            TMDB_BASE_URL + req + '?' +
                            /* adding parameters */
                            API_KEY_PAR + '=' + API_KEY + '&' +
                            PAGE_KEY_PAR + '=' + movieListPage;

        /* ... start HTTP async request ... */
        HTTPAsyncRequest requester = new HTTPAsyncRequest(nextMoviesListener);
        requester.execute(requestURL.toString());
    }

    public void getMovieTrailers(final int ID){
        /* place the movie ID in the request */
        String req = resources.getString(R.string.trailers_req);
        String req_id = req.replace(resources.getString(R.string.id_req), String.valueOf(ID));

        /* parsing request url */
        String requestURL = /* base url + request */
                            TMDB_BASE_URL + req_id + '?' +
                            /* adding parameters */
                            API_KEY_PAR + '=' + API_KEY;

        /* start HTTP async request */
        HTTPAsyncRequest requester = new HTTPAsyncRequest(movieTrailersListener);
        requester.execute(requestURL);
    }

    public void nextMovieReviews(final int ID){
        /* if we have reached the final page ... */
        if(movieReviewsPage == MOVIE_REVIEW_LAST_PAGE+1) {
            /* ... return without doing anything */
            return;
        }
        /* ... otherwise, we have to retrieve the new page ... */

        /* place the movie ID in the request */
        String req = resources.getString(R.string.reviews_req);
        String req_id = req.replace(resources.getString(R.string.id_req), String.valueOf(ID));

        /* parsing request url */
        String requestURL = /* base url + request */
                            TMDB_BASE_URL + req_id + '?' +
                            /* adding parameters */
                            API_KEY_PAR + '=' + API_KEY + '&' +
                            PAGE_KEY_PAR + '=' + movieReviewsPage;

        /* start HTTP async request */
        HTTPAsyncRequest requester = new HTTPAsyncRequest(nextReviewsListener);
        requester.execute(requestURL);
    }

    public void getMovie(final int ID){
        /* place the movie ID in the request */
        String req = resources.getString(R.string.movie_req);
        String req_id = req.replace(resources.getString(R.string.id_req), String.valueOf(ID));


        /* parsing request url */
        String requestURL = /* base url + request */
                            TMDB_BASE_URL + req_id + '?' +
                            /* adding parameters */
                            API_KEY_PAR + '=' + API_KEY;

        /* start HTTP async request */
        HTTPAsyncRequest requester = new HTTPAsyncRequest(new MovieListener(ID));
        requester.execute(requestURL);
    }
}
