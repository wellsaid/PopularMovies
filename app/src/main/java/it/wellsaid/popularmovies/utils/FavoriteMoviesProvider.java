package it.wellsaid.popularmovies.utils;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;

/* Adapted from the following example:
 *  https://github.com/udacity/android-content-provider
 */

public class FavoriteMoviesProvider extends ContentProvider{

    private static final String TAG = FavoriteMoviesProvider.class.getSimpleName();

    /* favorite movie ordering string in settings */
    public static final String FAVORITE_MOVIES = "favorite";

    /* Used to match the URI given by the caller */
    private static final UriMatcher uriMatcher = buildUriMatcher();
    /* The object which will perform queries to the database */
    private FavoriteMoviesDBHelper dbHelper;

    /* Codes for the UriMatcher */
    private static final int MOVIES = 100;
    private static final int MOVIE_ID = 101;

    private static UriMatcher buildUriMatcher() {
        /* Build a UriMatcher by adding a specific code to return based on a match */
        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        final String authority = FavoriteMoviesContract.CONTENT_AUTHORITY;

        /* add URI to recognize to our UriMatcher */
        matcher.addURI(authority, FavoriteMoviesContract.TABLE_FAV_MOVIES, MOVIES);
        matcher.addURI(authority, FavoriteMoviesContract.TABLE_FAV_MOVIES + "/#",
                MOVIE_ID);

        return matcher;
    }

    @Override
    public boolean onCreate() {
        dbHelper = new FavoriteMoviesDBHelper(getContext());

        return true;
    }

    @Override
    public String getType(@NonNull Uri uri) {
        /* get the request sanded by the caller */
        final int match = uriMatcher.match(uri);

        /* return the MIME type of this request */
        switch (match) {
            case MOVIES: /* request: all the favorite movies */
                return ContentResolver.CURSOR_DIR_BASE_TYPE + "/" +
                        FavoriteMoviesContract.CONTENT_AUTHORITY
                        + "/" + FavoriteMoviesContract.TABLE_FAV_MOVIES;
            case MOVIE_ID: /* request: a movie by its id */
                return ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" +
                        FavoriteMoviesContract.CONTENT_AUTHORITY
                        + "/" + FavoriteMoviesContract.TABLE_FAV_MOVIES;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
    }

    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        Cursor retCursor;
        switch(uriMatcher.match(uri)) {
            case MOVIES: /* request: all the favorite movies */
                /* perform the query and return the result in a cursor */
                retCursor = dbHelper.getReadableDatabase().query(
                        FavoriteMoviesContract.TABLE_FAV_MOVIES,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
                return retCursor;
            case MOVIE_ID: /* request: a movie by its id */
                /* perform the query and return the result in a cursor */
                retCursor = dbHelper.getReadableDatabase().query(
                        FavoriteMoviesContract.TABLE_FAV_MOVIES,
                        projection,
                        FavoriteMoviesContract.Entry._ID + " = ?",
                        new String[] {String.valueOf(ContentUris.parseId(uri))},
                        null,
                        null,
                        sortOrder);
                return retCursor;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
    }

    @Override
    public Uri insert(@NonNull Uri uri, ContentValues values) {
        final SQLiteDatabase db = dbHelper.getWritableDatabase();
        Uri returnUri;
        switch (uriMatcher.match(uri)) {
            case MOVIES: /* request: all the favorite movies */
                /* perform insertion of values */
                long _id = db.insert(FavoriteMoviesContract.TABLE_FAV_MOVIES,
                        null, values);

                /* if the insertion returned with a valid id ... */
                if (_id > 0) {
                    /* ... build uri for inserted movies */
                    returnUri = FavoriteMoviesContract.Entry.buildFavoriteMoviesUri(_id);
                /* ... otherwise ... */
                } else {
                    /* ... throws an exception */
                    throw new android.database.SQLException("Failed to insert row into: " + uri);
                }
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        /* notify change to the content resolver */
        getContext().getContentResolver().notifyChange(uri, null);
        return returnUri;
    }

    @Override
    public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {
        final SQLiteDatabase db = dbHelper.getWritableDatabase();
        final int match = uriMatcher.match(uri);
        int numDeleted;
        switch(match) {
            case MOVIES: /* request: all the favorite movies */
                /* perform deletion */
                numDeleted = db.delete(
                        FavoriteMoviesContract.TABLE_FAV_MOVIES, selection, selectionArgs);
                break;
            case MOVIE_ID: /* request: a movie by its id */
                /* perform deletion */
                numDeleted = db.delete(FavoriteMoviesContract.TABLE_FAV_MOVIES,
                        FavoriteMoviesContract.Entry._ID + " = ?",
                        new String[]{String.valueOf(ContentUris.parseId(uri))});
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        return numDeleted;
    }

    @Override
    public int bulkInsert(@NonNull Uri uri, @NonNull ContentValues[] values) {
        final SQLiteDatabase db = dbHelper.getWritableDatabase();
        final int match = uriMatcher.match(uri);
        switch(match) {
            case MOVIES: /* request: all the favorite movies */
                /* begin allows for multiple transactions */
                db.beginTransaction();

                int numInserted = 0;
                try {
                    /* for each value passed by the caller ... */
                    for(ContentValues value : values) {
                        if (value == null) {
                            throw new IllegalArgumentException("Cannot have null content values");
                        }

                        long _id = -1;
                        try {
                            /* ... perform insertion ... */
                            _id = db.insertOrThrow(FavoriteMoviesContract.TABLE_FAV_MOVIES,
                                    null, value);
                        } catch(SQLiteConstraintException e) {
                            Log.w(TAG, "Attempting to insert " +
                                    value.getAsString(
                                            FavoriteMoviesContract.Entry._ID)
                                    + " but value is already in database.");
                        }
                        /* if insertion has been successful ... */
                        if (_id != -1) {
                            /* ... increment number of inserted values */
                            numInserted++;
                        }
                    }
                    /* if the where no errors ... */
                    if(numInserted > 0) {
                        /* ... set transaction has successful */
                        db.setTransactionSuccessful();
                    }
                } finally {
                    /* at the end: end the transaction */
                    db.endTransaction();
                }
                /* if the where no errors ... */
                if (numInserted > 0) {
                    /* ... notify the content resolver of the change */
                    getContext().getContentResolver().notifyChange(uri, null);
                }
                return numInserted;
            default:
                return super.bulkInsert(uri, values);
        }
    }

    @Override
    public int update(@NonNull Uri uri, ContentValues contentValues, String selection, String[] selectionArgs) {
        final SQLiteDatabase db = dbHelper.getWritableDatabase();
        int numUpdated;

        if (contentValues == null) {
            throw new IllegalArgumentException("Cannot have null content values");
        }

        final int match = uriMatcher.match(uri);
        switch(match) {
            case MOVIES: /* request: all the favorite movies */
                /* perform update */
                numUpdated = db.update(FavoriteMoviesContract.TABLE_FAV_MOVIES,
                        contentValues,
                        selection,
                        selectionArgs);
                break;
            case MOVIE_ID: /* request: a movie by its id */
                /* perform update */
                numUpdated = db.update(FavoriteMoviesContract.TABLE_FAV_MOVIES,
                        contentValues,
                        FavoriteMoviesContract.Entry._ID + " = ?",
                        new String[] {String.valueOf(ContentUris.parseId(uri))});
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        /* if there where no errors ... */
        if (numUpdated > 0) {
            /* ... notify the content resolver of the change */
            getContext().getContentResolver().notifyChange(uri, null);
        }

        return numUpdated;
    }

}