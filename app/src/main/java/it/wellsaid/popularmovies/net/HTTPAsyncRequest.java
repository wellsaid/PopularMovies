package it.wellsaid.popularmovies.net;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class HTTPAsyncRequest extends AsyncTask<String, Void, String> {

    private static final String TAG = HTTPAsyncRequest.class.getSimpleName();

    /* the class that wants to receive the response must implements this interface ... */
    public interface HTTPAsyncRequestListener{
        /* ... and override this method */
        void onServerResponse(String response);
    }

    /* the listener to which send the response */
    private final HTTPAsyncRequestListener listener;

    public HTTPAsyncRequest(HTTPAsyncRequestListener listener){
        this.listener = listener;
    }

    @Override
    protected String doInBackground(String... params) {
        /* build the url */
        URL url;
        try {
            url = new URL(params[0]);
        } catch (MalformedURLException e){
            Log.v(TAG, "Malformed URL: " + e.getMessage());
            return "";
        }

        HttpURLConnection connection;
        try {
            /* starting http request */
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.connect();

            /* reading the response */
            BufferedReader rd;
            if(connection.getResponseCode() != 200){
                rd = new BufferedReader(new InputStreamReader(connection.getErrorStream()));
            } else {
                rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            }

            StringBuilder content = new StringBuilder();
            String line;
            while ((line = rd.readLine()) != null) {
                content.append(line).append('\n');
            }

            /* returning it to the caller */
            return content.toString();
        } catch (IOException e){
            e.printStackTrace();
            Log.v(TAG, "Problems connecting to the server: " + e.getMessage());
            return "";
        }
    }

    @Override
    protected void onPostExecute(String response) {
        /* return response to the listener */
        listener.onServerResponse(response);
    }
}
