# PopularMovies
Simple app that displays a list of popular movies and allows the user to add/remove movies from favorites.

Developed as a project for the Android Developer Nanodgree of udacity.com
